# Python Introduction
2023W 070128-1 Methodological course - Analyzing Central Europe's Intellectual Heritage

## General Shortcuts

`Ctrl + Shift + X`: extension manager

`Ctrl + Shift + P`: command palette

`Ctrl + Shift + G`: source control view

## Repository Setup
1. fork this repository in GitLab to have a private copy
2. open VS Code
3. clone git repository via command palette: `Git: Clone`
  - use `https` and not `ssh` with your username and password
  - if asked, open cloned repository in a new window
4. download an image from the internet and add it to this readme
5. make a git commit via the source control view and push the changes to GitLab

## Python Setup
1. create virtual environment `.venv` via command palette: `Python: Create environment`
2. install project requirements
  - open a new terminal via menu: (`Terminal > New Terminal`)
  - verify you are using the virtual environment
  - install dependencies via python packet manager `pip install -r requirements.txt`
  - list the installed packages via python packet manager `pip freeze`
  - close the terminal
3. open the first Jupyter Notebook and install package if needed

## Part 1 - Web Resources
This notebooks gives an introduction how to us data sources from the internet using the [`requests`](https://requests.readthedocs.io/en/latest/) library.

## Part 2 - XML Data
Parsing and analysis of TEI-XML data using [`lxml`](https://lxml.de/) library.

## Part 3 - CSV Data
Analysis of spreadsheets with [`pandas`](https://pandas.pydata.org/) data frames.

## Part 4 - Image Data
Manipulation of images with [`Pillow`](https://python-pillow.org/) and object detection using [`ultralytics`](https://www.ultralytics.com/)


# Test commit